module EcbHelper
  def self.getRates
    Rails.cache.fetch("rates/rates", expires_in: 24.hours) do
      getXml
    end
  end

  def self.getXml
    nodes = Hash.new()

    xml = Nokogiri::XML(open(Rails.configuration.ecb_url))
    xml.css('Cube Cube').first.children.each do |node|
      nodes[node['currency']] = node['rate']
    end

    return nodes
  end
end