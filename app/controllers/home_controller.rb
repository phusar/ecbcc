class HomeController < ApplicationController
  def index
    @products = Product.all
    @rates = EcbHelper.getRates
  end
end
