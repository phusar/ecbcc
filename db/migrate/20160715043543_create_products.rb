class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price

      t.timestamps
    end

    Product.create!(name: 'Testovaci produkt 1', price: 42.00)
    Product.create!(name: 'Testovaci produkt 2', price: 1.12)
    Product.create!(name: 'Testovaci produkt 3', price: 999.99)
    Product.create!(name: 'Testovaci produkt 4', price: 1.00)
    Product.create!(name: 'Testovaci produkt 5', price: 123.45)
    Product.create!(name: 'Testovaci produkt 6', price: 1)
  end
end
